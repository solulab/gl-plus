/*
 Copyright (c) 2016 Sachin Verma
 
 SVMenuViewController.swift
 SVSlidingPanel
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import UIKit

class SVMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var contentTableView: UITableView!
    
    let cellID = "MenuOptionTableViewCell"
    var keyboardSize = CGSize.zero
    var recognizer : UITapGestureRecognizer!
//    let menuItems = [SVMenuOptions.GLPLUS, SVMenuOptions.Donate, SVMenuOptions.ContactUs, SVMenuOptions.AboutUs, SVMenuOptions.Help, SVMenuOptions.OtherApps, SVMenuOptions.RateUs]
    let tableViewData = [["logo":"aboutus","label":"About Us"], ["logo":"contactus","label":"Contact Us"], ["logo":"donate","label":"Donate"], ["logo":"rateus","label":"Rate Us"], ["logo":"help","label":"Help"],["logo":"otherapps","label":"Other Apps"]]
    let textColor = [["TextColor":UIColor.init(red: 105/255, green: 163/255, blue: 213/255, alpha: 1)],["TextColor":UIColor.init(red: 140/255, green: 198/255, blue: 65/255, alpha: 1)], ["TextColor":UIColor.init(red: 235/255, green: 149/255, blue: 34/255, alpha: 1)], ["TextColor":UIColor.init(red: 159/255, green: 106/255, blue: 165/255, alpha: 1)], ["TextColor":UIColor.init(red: 80/255, green: 112/255, blue: 134/255, alpha: 1)], ["TextColor":UIColor.init(red: 129/255, green: 128/255, blue: 28/255, alpha: 1)]]
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
    contentTableView.reloadData()
    }
    
//    @objc func keyboardWillHide(_ notification: Notification)
//    {
//        keyboardSize = CGSize.zero
//        self.view.removeGestureRecognizer(recognizer)
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! menuCell
        let cellData = tableViewData[indexPath.row]
        cell.lbltitle.text = cellData["label"]!
        let image = UIImage(named: cellData["logo"]!)
        cell.imgView.image = image
        cell.lbltitle.textColor = textColor[indexPath.row]["TextColor"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        
        switch indexPath.row
        {
        case 0:
            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
            
            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
            break

//
//            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
//            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
//
//            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "DonateViewController") as! DonateViewController
//            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
//            break
        case 1:
            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
            
            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
            break
        case 2:
                        let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
                        let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
            
                        let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "DonateViewController") as! DonateViewController
                        currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
                    break
        case 3:
            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
            
            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "RateUsViewController") as! RateUsViewController
            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
            
            break

        case 4:
            
            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
            
            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
            break

//            let tabBarController = appDel.drawerContainer?.centerViewController as! CustomTabBarViewController
//            let currentSelectedTab = (tabBarController.selectedViewController as! UINavigationController).viewControllers[0] as! BaseNavigationViewController
//
//            let choosePreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "OthersAppViewController") as! OthersAppViewController
//            currentSelectedTab.navigationController?.pushViewController(choosePreferencesVC, animated: false)
            
            break
        case 5:
            UIApplication.shared.openURL(URL(string : "https://itunes.apple.com/in/developer/arnion-technologies-private-limited/id660611788")!)

        default:
            break
        }
        
        appDel.drawerContainer?.closeDrawer(animated: true, completion: { (isClosed: Bool) in
            
            
        })
    }
}

class menuCell: UITableViewCell {
    @IBOutlet var lbltitle : UILabel!
    @IBOutlet var imgView : UIImageView!
}
