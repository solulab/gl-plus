//
//  IdiomsDetailsViewController.swift
//  GL Plus
//
//  Created by Rajat on 06/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import GoogleMobileAds

class IdiomsDetailsViewController: UIViewController, GADBannerViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var gadBannerObj: GADBannerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblWord: UILabel!
   
    var currentObject : [String : String]?
    override func viewDidLoad() {
        super.viewDidLoad()
     setUp()
     tableView.tableFooterView = UIView()
        
        lblWord.text = currentObject?["strTxtEnter"]
        if currentObject?.count == 0
        {
         tableView.isHidden = true
        }
        else
        {
         tableView.isHidden = false
        }
        

        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
    }
    
    func setUp(){
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Idioms"
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(btnBackPress(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "share"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnSharePress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItems = [barButton1]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSharePress(sender:UIButton)
    {
        let text = "Hey, I found this interesting! Check this out!\n\n"
        let appLink = "\(SHARELINK)\(lblWord.text!)&type=1&page=0"
        print(appLink)
        let textToShare = [ text, appLink ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath ) as! TableViewPharsesCell
        cell.lblTitle.text = currentObject!["strWord"]
        cell.lblmeaning.text = currentObject!["strMeaning"]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class TableViewPharsesCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblmeaning : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let separator = UIView(frame: CGRect(x: 0, y: bounds.size.height-10, width: bounds.size.width, height: 10))
//        separator.backgroundColor = UIColor.clear
//        contentView.addSubview(separator)
    }
    
}
