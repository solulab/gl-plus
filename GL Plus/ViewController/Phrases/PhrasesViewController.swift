//
//  PhrasesViewController.swift
//  GL Plus
//
//  Created by Hetal Govani on 16/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import SwiftyXMLParser
import ReachabilitySwift

class PhrasesViewController: BaseNavigationViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate
{
    
    
    @IBOutlet var txtsearchBox: UITextField!
    var customKeyboard: PMCustomKeyboard?
    var pkcustomKeyboard: PKCustomKeyboard?
    var keyboardSize = CGSize.zero
    var recognizer : UITapGestureRecognizer!
    var arrTopSearchsWords : Array<Dictionary<String,AnyObject>> = Array()
    var arrRecentSearchesWords : Array<Dictionary<String,AnyObject>> = Array()
    var rechabilityObj = Reachability()!
    
    @IBOutlet var collectionTopSearchsWords : UICollectionView!
    @IBOutlet var collectionRecentSearchesWords : UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLoaderView()
        self.title = "Phrases"
        NotificationCenter.default.addObserver(self, selector: #selector(self.rechabilitychange), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
        rechabilityObj = Reachability()!
        try! rechabilityObj.startNotifier()
        if UI_USER_INTERFACE_IDIOM() == .phone {
            customKeyboard = PMCustomKeyboard()
            customKeyboard?.textView = txtsearchBox
        }
        else {
            pkcustomKeyboard = PKCustomKeyboard()
            pkcustomKeyboard?.textView = txtsearchBox
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        XMLParsingForTop()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let abc = USERDEFAULT.value(forKey: KPHARSE) as? [[String : String]]
        {
            phrase.sharedInstance.arrRecentphrase = abc
            if phrase.sharedInstance.arrRecentphrase.count>5
            {
                let otherRange = phrase.sharedInstance.arrRecentphrase.index(phrase.sharedInstance.arrRecentphrase.startIndex, offsetBy: 5)..<phrase.sharedInstance.arrRecentphrase.endIndex
                phrase.sharedInstance.arrRecentphrase.removeSubrange(otherRange)
                
            }
            self.collectionRecentSearchesWords.reloadData()
        }
        //        if phrase.sharedInstance.arrRecentphrase.count > 0 {
        //            self.collectionRecentSearchesWords.reloadData()
        //        }
        if phrase.sharedInstance.arrTopphrase.count > 0
        {
            self.collectionTopSearchsWords.reloadData()
        }
    }
    
    @objc func rechabilitychange(_ notification: Notification) {
        if rechabilityObj.isReachable == true {
            
            
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
    }
    
    //mark:- XML parsing
    func XMLParsing(strTxtEnter:String)
    {
        self.showHud()
        let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> \n" +
            "<SOAP-ENV:Body>\n" +
            "<m:DicSpecial xmlns:m=\"urn:glwsdl\">\n" +
            "<expression xsi:type=\"xsd:string\">\(strTxtEnter)</expression>\n" +
            "<dictionary xsi:type=\"xsd:string\">Phrases</dictionary>\n" +
            "<start xsi:type=\"xsd:string\">1</start>\n" +
            "<offset xsi:type=\"xsd:string\">2</offset>\n" +
            "</m:DicSpecial>\n" +
            "</SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        var error: NSError?
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                // Assign Value to class
                if((xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.count) != nil)
                {
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            let strMeaning = ele.childElements[1].text!
                            
                            newElement["strTxtEnter"] = strTxtEnter
                            newElement["strWord"] = strWord
                            newElement["strMeaning"] = strMeaning
                            
                            phrase.sharedInstance.arrRecentphrase.insert(newElement, at: 0)
                            USERDEFAULT.set(phrase.sharedInstance.arrRecentphrase, forKey: KPHARSE)
                            USERDEFAULT.synchronize()
                            break
                        }
                    }
                    DispatchQueue.main.async {
                        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhrasesDetailsViewController") as! PhrasesDetailsViewController
                        donetViewObj.currentObject = phrase.sharedInstance.arrRecentphrase.first
                        self.navigationItem.leftBarButtonItems = nil
                        self.removeHud()
                        self.navigationController?.pushViewController(donetViewObj, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        print("No Word Found from your selection")
                        UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                            //Do some Action.
                            }
                        }
                        self.removeHud()
                    }
                }
                }
                
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    var arrForTopSearch :[String] = []
    func XMLParsingForTop()
    {
        self.showHud()
        
        let is_SoapMessage = """
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <SOAP-ENV:Body> <m:TopSearch xmlns:m="urn:glwsdl"> <limit xsi:type="xsd:string">5</limit> <dictionary xsi:type="xsd:string">Phrases</dictionary> </m:TopSearch> </SOAP-ENV:Body> </soap:Envelope>
"""
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                
                let strWord = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.text
                
                if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                {
                    self.arrForTopSearch.removeAll()
                    
                    for ele in elementsArray
                    {
                        var newElement = [String : String]()
                        let strWord = ele.childElements[0].text!
                        self.arrForTopSearch.insert(strWord, at: self.arrForTopSearch.count)
                    }
                    DispatchQueue.main.async {
                        self.removeHud()
                        self.tbl.reloadData()
                        
                    }
                    
                }
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int // Default is 1 if not implemented
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrForTopSearch.count == 0){
            return 0
        }else{
            
            return arrForTopSearch.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! custPharseCell
        cell.txtPharseTop.text = "\(arrForTopSearch[indexPath.row])"
        return cell
    }

    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    @IBOutlet weak var tbl: UITableView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionRecentSearchesWords
        {
            return phrase.sharedInstance.arrRecentphrase.count
            
        }
        else
        {
            return phrase.sharedInstance.arrTopphrase.count
            
        }
        //        return 5
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        return CGSize(width: collectionView.frame.width/2.5, height: 200);
        return CGSize(width: collectionView.frame.width/2.5, height: 110/140 * (collectionView.frame.width/1.5));
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MyCollectionViewPhrasesCell
        
        if collectionView == collectionRecentSearchesWords
        {
            let element = phrase.sharedInstance.arrRecentphrase[indexPath.row]
            cell.lblTitle.text = element["strTxtEnter"]
            cell.lblType.text = element["strWord"]
            cell.lblPhrasesTitle.text = element["strMeaning"]
        }
        else
        {
            cell.lblTitle.text = phrase.sharedInstance.Word
            cell.lblType.text = phrase.sharedInstance.arrTopphrase[indexPath.row].childElements.first?.text
            cell.lblPhrasesTitle.text = phrase.sharedInstance.arrTopphrase[indexPath.row].childElements[1].text
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhrasesDetailsViewController") as! PhrasesDetailsViewController
        if collectionView == collectionRecentSearchesWords {
            
            let object = phrase.sharedInstance.arrRecentphrase[indexPath.row]
            donetViewObj.currentObject = object
        }
        else{
            phrase.sharedInstance.Word = (phrase.sharedInstance.arrTopphrase[indexPath.row].childElements.first?.text)!
            phrase.sharedInstance.meaning = (phrase.sharedInstance.arrTopphrase[indexPath.row].childElements.first?.text)!
            
        }
        self.navigationItem.leftBarButtonItems = nil
        self.navigationController?.pushViewController(donetViewObj, animated: true)
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        if txtsearchBox.text?.characters.count == 0
        {
            txtsearchBox.resignFirstResponder()
            
        }
        if rechabilityObj.isReachable == true
        {
            txtsearchBox.resignFirstResponder()
            XMLParsing(strTxtEnter:txtsearchBox.text!)
            txtsearchBox.text = ""
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
        
    

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        txtsearchBox.resignFirstResponder()
        
        if txtsearchBox.text?.characters.count == 0
        {
            txtsearchBox.resignFirstResponder()
        }
        if rechabilityObj.isReachable == true
        {
            txtsearchBox.resignFirstResponder()
            
            let str = "\(arrForTopSearch[indexPath.row])"
            XMLParsing(strTxtEnter:str)
            txtsearchBox.text = ""
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
        
        
    }

    //mark: - TextField delegate
    func textFieldShouldReturn(_ txtValue: UITextField) -> Bool {
        if txtValue == txtsearchBox {
            if txtValue.text?.characters.count == 0
            {
                txtValue.resignFirstResponder()
                
                return false
            }
            if rechabilityObj.isReachable == true
            {
                txtsearchBox.resignFirstResponder()
                XMLParsing(strTxtEnter:txtsearchBox.text!)
                txtsearchBox.text = ""
            }
            else
            {
                UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                    //Do some Action.
                    }
                }
            }
            
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "")
        {
            return false
        }
        return true
    }
    func textFieldDidChange(_ txtValue: UITextField)
    {
        if (txtValue.text == "અૈ") {
            txtValue.text = "ઐ"
            txtsearchBox.text = "ઐ"
        }
        if (txtValue.text == "અૅા") || (txtValue.text == "અાૅ") || (txtValue.text == "આૅ") {
            txtValue.text = "ઑ"
            txtsearchBox.text = "ઑ"
        }
        if (txtValue.text == "આૈ") || (txtValue.text == "અૌ") || (txtValue.text == "ઐા") || (txtValue.text == "ઐા") {
            txtValue.text = "ઔ"
            txtsearchBox.text = "ઔ"
        }
        if (txtValue.text == "અા") {
            txtValue.text = "આ"
            txtsearchBox.text = "આ"
        }
        if (txtValue.text == "અે") {
            txtValue.text = "એ"
            txtsearchBox.text = "એ"
        }
        if (txtValue.text == "આે") || (txtValue.text == "એા") || (txtValue.text == "અો") {
            txtValue.text = "ઓ"
            txtsearchBox.text = "ઓ"
        }
        if (txtValue.text == "અૅ") {
            txtValue.text = "ઍ"
            txtsearchBox.text = "ઍ"
        }
        
        var code: String = ""
        if (txtsearchBox.text?.characters.count)! >= 2 {
            //            code = (txtsearchBox.text as? NSString)?.substring(from: txtsearchBox.text.length() - 2)
            
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            //            let index = txtsearchBox.text!.index(txtsearchBox.text!.endIndex, offsetBy: 2)
            
            code = (txtsearchBox.text?.substring(from: index!))!
        }
        if (code == "ાૅ") || (code == "ૅા") {
            code = "ૉ"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        if (code == "ેા") || (code == "ાે") {
            code = "ો"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        
        
    }
    
    // MARK: - Tap Gesture
    @objc func tap(gesture: UITapGestureRecognizer)
    {
        if keyboardSize != CGSize.zero {
            txtsearchBox.resignFirstResponder()
            
        }
    }
    // MARK: - Key Board Show & Hidden Method
    @objc func keyboardWillShow(_ notification: Notification) {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(PhrasesViewController.tap(gesture:)))
        self.view.addGestureRecognizer(recognizer)
        keyboardSize = ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size)!
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        keyboardSize = CGSize.zero
        self.view.removeGestureRecognizer(recognizer)
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}

class MyCollectionViewPhrasesCell: UICollectionViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet var lblPhrasesTitle : UILabel!
    
}

