//
//  CustomTabBarViewController.swift
//  AN Client
//
//  Created by Hetal Govani on 02/08/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

protocol CustomTabBarControllerDelegate
{
    func customTabBarControllerDelegate_CenterButtonTapped(tabBarController:CustomTabBarViewController, button:UIButton, buttonState:Bool);
}

class CustomTabBarViewController: UITabBarController, UITabBarControllerDelegate
{
    var customTabBarControllerDelegate:CustomTabBarControllerDelegate?;
    var centerButton:UIButton!;
    var centerButtonTappedOnce:Bool = false;
    var comeFrom:String = ""
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews();
//        self.bringcenterButtonToFront();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        selectedMenuItem = 1
        self.delegate = self;
        self.tabBar.backgroundColor = greenColor
//      let numberOfItems = CGFloat((self.tabBar.items!.count))
        
        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / 5, height: (self.tabBar.frame.height))
//        self.tabBar.tintColor = brownColor
        self.tabBar.selectionIndicatorImage = UIImage().imageWithColor(color: greenColor, size: tabBarItemSize)

//        let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "OppositesViewController") as! OppositesViewController
        
//        let dashboardItem = self.tabBar.items![0] as UITabBarItem
//        dashboardItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        
//        let dashboardItemImage = UIImage(named:"tab1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let dashboardItemImageSelected = UIImage(named: "tab1")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        dashboardItem.image = dashboardItemImage
//        dashboardItem.selectedImage = dashboardItemImageSelected.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
////        self.tabBar.frame.size.width = self.view.frame.width + 4
////        self.tabBar.frame.origin.x = -2
//
//        let nav1 = UINavigationController(rootViewController: dashboardVC)
//        let view2Obj = self.storyboard?.instantiateViewController(withIdentifier: "IdiomsViewController") as! IdiomsViewController
//        let view2Item = self.tabBar.items![1] as UITabBarItem
//        view2Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
//        let view2ItemImage = UIImage(named:"tab2")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let view2ItemImageSelected = UIImage(named: "tab2")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        view2Item.image = view2ItemImage
//        view2Item.selectedImage = view2ItemImageSelected
//
//        let nav2 = UINavigationController(rootViewController: view2Obj)
//        let view3Obj = self.storyboard?.instantiateViewController(withIdentifier: "PhrasesViewController") as! PhrasesViewController
//        let view3Item = self.tabBar.items![2] as UITabBarItem
//        view3Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
//        let view3ItemImage = UIImage(named:"tab3")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let view3ItemImageSelected = UIImage(named: "tab3")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        view3Item.image = view3ItemImage
//        view3Item.selectedImage = view3ItemImageSelected
//
//        let nav3 = UINavigationController(rootViewController: view3Obj)
//        let view4Obj = self.storyboard?.instantiateViewController(withIdentifier: "ThesaurusViewController") as! ThesaurusViewController
//        let view4Item = self.tabBar.items![3] as UITabBarItem
//        view4Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
//        let view4ItemImage = UIImage(named:"tab4")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let view4ItemImageSelected = UIImage(named: "tab4")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        view4Item.image = view4ItemImage
//        view4Item.selectedImage = view4ItemImageSelected
//
//        let nav4 = UINavigationController(rootViewController: view4Obj)
//        let view5Obj = self.storyboard?.instantiateViewController(withIdentifier: "ProverbsViewController") as! ProverbsViewController
//        let view5Item = self.tabBar.items![4] as UITabBarItem
//        view5Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
//        let view5ItemImage = UIImage(named:"tab5")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let view5ItemImageSelected = UIImage(named: "tab5")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        view5Item.image = view5ItemImage
//        view5Item.selectedImage = view5ItemImageSelected
        
//        let nav5 = UINavigationController(rootViewController: view5Obj)
//        viewControllers = [nav1, nav2, nav3, nav4, nav5]
//        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification), name: notificationName, object: nil)
//        self.setupMiddleButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTabBarVisible(visible: false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setTabBarVisible(visible: true, animated: true)
    }
    
//    @objc func methodOfReceivedNotification()
//    {
//        let currentSelectedTab = (self.selectedViewController as! UINavigationController).viewControllers[0]
////        let newViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProverbsDetailsViewController") as! ProverbsDetailsViewController
//        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProverbsDetailsViewController") as! ProverbsDetailsViewController
//        currentSelectedTab.navigationController?.pushViewController(donetViewObj, animated: false)
////        SVMenuOptionManager.sharedInstance.slidingPanel.centerPanel = newViewController
//
////        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
////        let initialViewController = self.storyboard!.instantiateViewController(withIdentifier: "ProverbsDetailsViewController")
////        appDelegate.window?.rootViewController = initialViewController
////        appDelegate.window?.makeKeyAndVisible()
//
//    }
    
    // MARK: - TabbarDelegate Methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        switch viewController
        {
        case is OppositesViewController: break
//            self.showCenterButton()
        case is IdiomsViewController: break
//            self.showCenterButton()
        case is PhrasesViewController: break
//            self.showCenterButton()
        case is ThesaurusViewController: break
//            self.showCenterButton()
        case is ProverbsViewController: break
        default: break
//            self.showCenterButton()
        }
    }
    
    // MARK: - Internal Methods
    
    @objc private func centerButtonAction(sender: UIButton)
    {
        //        selectedIndex = 2
        if(!centerButtonTappedOnce)
        {
            centerButtonTappedOnce=true;
//            centerButton.setImage(UIImage(named: "ic_bullseye_white"), for: .normal)
        }
        else
        {
            centerButtonTappedOnce=false;
//            centerButton.setImage(UIImage(named: "ic_bullseye_red"), for: .normal)
        }
        
        customTabBarControllerDelegate?.customTabBarControllerDelegate_CenterButtonTapped(tabBarController: self,button: centerButton,buttonState: centerButtonTappedOnce);
    }
    
    func hideCenterButton()
    {
        centerButton.isHidden = true;
    }
    
    func showCenterButton()
    {
        centerButton.isHidden = false;
        self.bringcenterButtonToFront();
    }
    
    // MARK: - Private methods
    
    private func setupMiddleButton()
    {
        centerButton = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        
        var centerButtonFrame = centerButton.frame
        centerButtonFrame.origin.y = view.bounds.height - centerButtonFrame.height
        centerButtonFrame.origin.x = view.bounds.width/2 - centerButtonFrame.size.width/2
        centerButton.frame = centerButtonFrame
        
        centerButton.backgroundColor = greenColor
        centerButton.layer.cornerRadius = centerButtonFrame.height/2
        view.addSubview(centerButton)
        
        centerButton.setImage(UIImage(named: "tab3"), for: .normal)
//        centerButton.setImage(UIImage(named: "tab3"), for: .highlighted)
        centerButton.addTarget(self, action: #selector(centerButtonAction(sender:)), for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
    func bringcenterButtonToFront()
    {
        print("bringcenterButtonToFront called...")
        centerButton.setImage(UIImage(named: "tab3"), for: .normal)
//        centerButton.setImage(UIImage(named: "tab3"), for: .highlighted)
        self.view.bringSubview(toFront: self.centerButton);
    }
    
}
extension UIImage
{
    
    func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension CustomTabBarViewController {
    // pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion
    func setTabBarVisible(visible: Bool, animated: Bool, completion: ((Bool)->Void)? = nil ) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) {
            if let completion = completion {
                return completion(true)
            }
            else {
                return
            }
        }
        
        // get a frame calculation ready
        let height = self.tabBar.frame.size.height
        let offsetY = (visible ? -height : height)
        
        // zero duration means no animation
        let duration = (animated ? 0.5 : 0.0)
        
        UIView.animate(withDuration: duration, animations: {
            let frame = self.tabBar.frame
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
        }, completion:completion)
    }
    
    func tabBarIsVisible() -> Bool {
        return self.tabBar.frame.origin.y < view.frame.maxY
    }
    
    
}

