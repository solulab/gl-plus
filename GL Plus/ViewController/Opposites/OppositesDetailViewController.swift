//
//  OppositesDetailViewController.swift
//  GL Plus
//
//  Created by Rajat on 06/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftyXMLParser
import ReachabilitySwift


class OppositesDetailViewController: UIViewController, GADBannerViewDelegate {
    @IBOutlet var gadBannerObj: GADBannerView!
    
    @IBOutlet weak var lblWord: UILabel!
    @IBOutlet weak var lblWordType: UILabel!
    @IBOutlet weak var lblWordMeaning: UILabel!
    @IBOutlet weak var lbloppWord: UILabel!
    @IBOutlet weak var lbloppositeWordType: UILabel!
    @IBOutlet weak var lbloppomeaning: UILabel!
    
    
    @IBOutlet weak var btnIdioms: UIButton!
    @IBOutlet weak var btnPharses: UIButton!
    @IBOutlet weak var btnThesaurus: UIButton!
    @IBOutlet weak var btnProverbs: UIButton!
    
    var rechabilityObj = Reachability()!
    
    var currentObject : [String : String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.rechabilitychange), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
        rechabilityObj = Reachability()!
        try! rechabilityObj.startNotifier()
        
        
        
        lblWord.text = currentObject!["strWord"]
        lblWordType.text = currentObject!["strType"]
        lblWordMeaning.text = currentObject!["strMeaning"]
        lbloppWord.text = currentObject!["strOppword"]
        lbloppositeWordType.text = currentObject!["strOppType"]
        lbloppomeaning.text = currentObject!["stroppMeaning"]
        
        // set Button Title
        btnIdioms.setTitle(lblWord.text! + " - Idioms", for: .normal)
        btnPharses.setTitle(lblWord.text! + " - Pharses", for: .normal)
        btnThesaurus.setTitle(lblWord.text! + " - Thesaurus", for: .normal)
        btnProverbs.setTitle(lblWord.text! + " - Proverbs", for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUp()
    }
    
    func setUp(){
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Opposites"
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(btnBackPress(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "share"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnSharePress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItems = [barButton1]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
    }
    
    @objc func rechabilitychange(_ notification: Notification) {
        if rechabilityObj.isReachable == true {
            
            
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
    }
    
    //mark: XML IDIOMS
    func XMLParsingIdioms()
    {
        let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> \n" +
            "<SOAP-ENV:Body>\n" +
            "<m:DicSpecial xmlns:m=\"urn:glwsdl\">\n" +
            "<expression xsi:type=\"xsd:string\">\(String(describing: lblWord.text!))</expression>\n" +
            "<dictionary xsi:type=\"xsd:string\">Idioms</dictionary>\n" +
            "<start xsi:type=\"xsd:string\">1</start>\n" +
            "<offset xsi:type=\"xsd:string\">2</offset>\n" +
            "</m:DicSpecial>\n" +
            "</SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        var error: NSError?
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                 if((xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.count) != 0)
                {
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            let strMeaning = ele.childElements[1].text!
                            
                            newElement["strTxtEnter"] = self.lblWord.text!
                            newElement["strWord"] = strWord
                            newElement["strMeaning"] = strMeaning
                            
                            
                            Idioms.sharedInstance.arrRecentIdioms.insert(newElement, at: 0)
                        }
                    }
                    DispatchQueue.main.async {
                        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IdiomsDetailsViewController") as! IdiomsDetailsViewController
                        donetViewObj.currentObject = Idioms.sharedInstance.arrRecentIdioms.first
                        self.navigationItem.leftBarButtonItems = nil
                        self.navigationController?.pushViewController(donetViewObj, animated: true)
                    }
                }
                else
                 {
                    DispatchQueue.main.async {
                        print("No Word Found from your selection")
                        UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                            //Do some Action.
                            }
                        }
                    }
                }
                    }
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    
    //mark:- XML parsing Pharses
    func XMLParsingPharses()
    {
        let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> \n" +
            "<SOAP-ENV:Body>\n" +
            "<m:DicSpecial xmlns:m=\"urn:glwsdl\">\n" +
            "<expression xsi:type=\"xsd:string\">\(lblWord.text!)</expression>\n" +
            "<dictionary xsi:type=\"xsd:string\">Phrases</dictionary>\n" +
            "<start xsi:type=\"xsd:string\">1</start>\n" +
            "<offset xsi:type=\"xsd:string\">2</offset>\n" +
            "</m:DicSpecial>\n" +
            "</SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        var error: NSError?
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                if((xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.count) != nil)
                {
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            let strMeaning = ele.childElements[1].text!
                            
                            newElement["strTxtEnter"] = self.lblWord.text!
                            newElement["strWord"] = strWord
                            newElement["strMeaning"] = strMeaning
                            
                            phrase.sharedInstance.arrRecentphrase.insert(newElement, at: 0)
       
                        }
                    }
                    DispatchQueue.main.async {
                        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhrasesDetailsViewController") as! PhrasesDetailsViewController
                        donetViewObj.currentObject = phrase.sharedInstance.arrRecentphrase.first
                        self.navigationItem.leftBarButtonItems = nil
                        self.navigationController?.pushViewController(donetViewObj, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        print("No Word Found from your selection")
                        UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                            //Do some Action.
                            }
                        }
                    }
                }
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    
    //mark : Thesaurus XML parsing
    func XMLParsingThesaurus()
    {
        let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> \n" +
            "<SOAP-ENV:Body>\n" +
            "<m:ThesaurusSearch xmlns:m=\"urn:glwsdl\">" +
            "<expression xsi:type=\"xsd:string\">\(String(describing: lblWord.text!))</expression>\n" +
            "<dictionary xsi:type=\"xsd:string\">Thesaurus</dictionary>\n" +
            "<start xsi:type=\"xsd:string\">1</start>\n" +
            "<offset xsi:type=\"xsd:string\">2</offset>\n" +
            "</m:ThesaurusSearch>\n" +
            "</SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        var error: NSError?
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                 if((xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.count) != nil)
                {
                    
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        var newElement = [String : String]()
                        newElement["strTxtEnter"] = self.lblWord.text!
                        
                        for var i in (0..<elementsArray.count)
                        {
                            let ele = elementsArray[i]
                            let strSuggestedWord1 = ele.childElements[0].text!
                            newElement["strSuggestedWord\(i)"] = strSuggestedWord1
                        }
                        Thesaurus.sharedInstance.arrRecentThesaurus.insert(newElement, at: 0)
                    }
                    DispatchQueue.main.async {
                        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ThesaurusDetailsViewController") as! ThesaurusDetailsViewController
                        self.navigationItem.leftBarButtonItems = nil
                        donetViewObj.currentObject = Thesaurus.sharedInstance.arrRecentThesaurus.first
                        self.navigationController?.pushViewController(donetViewObj, animated: true)
                    }
                }
                else
                 {
                    DispatchQueue.main.async {
                        print("No Word Found from your selection")
                        UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                            //Do some Action.
                            }
                        }
                    }
                }
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    
    //mark: proverbs XML parsing
    func ProverbsXMLParsing()
    {
        let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<soap:Envelope xmlns:SOAP-ENV=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> \n" +
            "<SOAP-ENV:Body>\n" +
            "<m:DicSpecial xmlns:m=\"urn:glwsdl\">\n" +
            "<expression xsi:type=\"xsd:string\">\(String(describing: lblWord.text!))</expression>\n" +
            "<dictionary xsi:type=\"xsd:string\">ProverbG2G</dictionary>\n" +
            "<start xsi:type=\"xsd:string\">1</start>\n" +
            "<offset xsi:type=\"xsd:string\">2</offset>\n" +
            "</m:DicSpecial>\n" +
            "</SOAP-ENV:Body>\n" +
        "</soap:Envelope>"
        let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
        let session = URLSession.shared
        var error: NSError?
        
        lobj_Request.httpMethod = "POST"
        let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
        lobj_Request.httpBody = soapBody
        lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
            print("Response: \(String(describing: response))")
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")
            do
            {
                if(response != nil){

                let xml = try! XML.parse(data!) as? XML.Accessor
                print(xml!)
                if((xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.count) != nil)
                {
                    
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            let strMeaning = ele.childElements[1].text!
                            
                            newElement["strTxtEnter"] = self.lblWord.text!
                            newElement["strWord"] = strWord
                            newElement["strMeaning"] = strMeaning
                            Proverbs.sharedInstance.arrRecentProverbs.insert(newElement, at: 0)
                            
                        }
                    }
                    
                    DispatchQueue.main.async {
                        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProverbsDetailsViewController") as! ProverbsDetailsViewController
                        self.navigationItem.leftBarButtonItems = nil
                        donetViewObj.currentObject = Proverbs.sharedInstance.arrRecentProverbs.first
                        self.navigationController?.pushViewController(donetViewObj, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        print("No Word Found from your selection")
                        UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                            //Do some Action.
                            }
                        }
                    }
                }
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
            
        })
        task.resume()
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSharePress(sender:UIButton)
    {
        let text = "Hey, I found this interesting! Check this out!\n\n"
        let appLink = "\(SHARELINK)\(lblWord.text!)&type=1&page=0"
        print(appLink)
        let textToShare = [ text, appLink ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnIdiomsPressed(_ sender: UIButton) {
        if rechabilityObj.isReachable == true {
            
            XMLParsingIdioms()
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
        
    }
    
    @IBAction func btnPharsesPressed(_ sender: UIButton) {
        if rechabilityObj.isReachable == true
        {
            XMLParsingPharses()
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                
                }
                
            }
        }
    }
    
    
    @IBAction func btnThesaurusPressed(_ sender: UIButton) {
        
        if rechabilityObj.isReachable == true
        {
            XMLParsingThesaurus()
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                
                }
                
            }
        }
        
    }
    
    @IBAction func btnProverbsPressed(_ sender: UIButton) {
        if rechabilityObj.isReachable == true
        {
            ProverbsXMLParsing()
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                
                }
                
            }
        }
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
