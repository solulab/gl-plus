//
//  OppositesViewController.swift
//  GL Plus
//
//  Created by Hetal Govani on 16/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MMDrawerController
import SwiftyXMLParser
import ReachabilitySwift

class OppositesViewController: BaseNavigationViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet weak var txtsearchBox: customTextField!
    @IBOutlet weak var tbl: UITableView!
    var customKeyboard: PMCustomKeyboard?
    var pkcustomKeyboard: PKCustomKeyboard?
    var keyboardSize = CGSize.zero
    var recognizer : UITapGestureRecognizer!
    var arrTopSearchsWords : Array<Dictionary<String,AnyObject>> = Array()
    var arrRecentSearchesWords : Array<Dictionary<String,AnyObject>> = Array()
    var rechabilityObj = Reachability()!
    var arrForTopSearch :[String] = []
    @IBOutlet var collectionTopSearchsWords : UICollectionView!
    @IBOutlet var collectionRecentSearchesWords : UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addLoaderView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.rechabilitychange), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
        rechabilityObj = Reachability()!
        try! rechabilityObj.startNotifier()
        if UI_USER_INTERFACE_IDIOM() == .phone {
            customKeyboard = PMCustomKeyboard()
            customKeyboard?.textView = txtsearchBox
        }
        else
        {
            pkcustomKeyboard = PKCustomKeyboard()
            pkcustomKeyboard?.textView = txtsearchBox
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        XMLParsingForTop()

    }

    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
        if let abc = USERDEFAULT.value(forKey: KOPPOSITE) as? [[String : String]]
        {
        Opposite.sharedInstance.arrRecentOpposite = abc
            if Opposite.sharedInstance.arrRecentOpposite.count>5
            {
                let otherRange = Opposite.sharedInstance.arrRecentOpposite.index(Opposite.sharedInstance.arrRecentOpposite.startIndex, offsetBy: 5)..<Opposite.sharedInstance.arrRecentOpposite.endIndex
                Opposite.sharedInstance.arrRecentOpposite.removeSubrange(otherRange)
                
            }
        self.collectionRecentSearchesWords.reloadData()
        }

        if Opposite.sharedInstance.arrTopOpposite.count > 0
        {
            self.collectionTopSearchsWords.reloadData()
        }
    }
    
    @objc func rechabilitychange(_ notification: Notification) {
        if rechabilityObj.isReachable == true {
            
            
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
    }
    func XMLParsing(strTxtEnter:String)
    {
        let reachability = Reachability()!
        if reachability.isReachable
        {
            self.showHud()
            let is_SoapMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> \n" +
                "<SOAP-ENV:Body>\n" +
                "<m:DicSpecial xmlns:m=\"urn:glwsdl\">\n" +
                "<expression xsi:type=\"xsd:string\">\(strTxtEnter)</expression>\n" +
                "<dictionary xsi:type=\"xsd:string\">Opposites</dictionary>\n" +
                "<start xsi:type=\"xsd:string\">1</start>\n" +
                "<offset xsi:type=\"xsd:string\">2</offset>\n" +
                "</m:DicSpecial>\n" +
                "</SOAP-ENV:Body>\n" +
            "</SOAP-ENV:Envelope>"
            let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
            let session = URLSession.shared
            var error: NSError?
            
            lobj_Request.httpMethod = "POST"
            let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
            lobj_Request.httpBody = soapBody
            lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
            
            let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
                print("Response: \(String(describing: response))")
                let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Body: \(String(describing: strData))")
                do
                {
                    if(response != nil){

                    let xml = try! XML.parse(data!) as? XML.Accessor
                    print(xml!)
                    
                    let strWord = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.text
                    
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            let strMeaning = ele.childElements[1].text!
                            let strType = ele.childElements[2].text!
                            let strOppword = ele.childElements[3].text!
                            let strOppType = ele.childElements[4].text!
                            let stroppMeaning = ele.childElements[5].text!
                            
                            newElement["strWord"] = strWord
                            newElement["strMeaning"] = strMeaning
                            newElement["strType"] = strType
                            newElement["strOppword"] = strOppword
                            newElement["strOppType"] = strOppType
                            newElement["stroppMeaning"] = stroppMeaning
                            
                            Opposite.sharedInstance.arrRecentOpposite.insert(newElement, at: 0)
                            USERDEFAULT.set(Opposite.sharedInstance.arrRecentOpposite, forKey: KOPPOSITE)
                            USERDEFAULT.synchronize()
                            break
                        }
                    }
                    
                    // Assign Value to class
                    if((strWord?.characters.count) != nil)
                    {
                        DispatchQueue.main.async {
                            
                            
                            let donetViewObj = self.storyboard?.instantiateViewController(withIdentifier: "OppositesDetailViewController") as! OppositesDetailViewController
                            donetViewObj.currentObject = Opposite.sharedInstance.arrRecentOpposite.first
                            self.navigationItem.leftBarButtonItems = nil
                            self.removeHud()
                            self.navigationController?.pushViewController(donetViewObj, animated: true)
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            print("No Word Found from your selection")
                            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "No Word Found from your selection", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                                //Do some Action.
                                }
                            }
                            self.removeHud()
                        }
                    }
                    }
                }
                catch
                {
                    print(error.localizedDescription)
                }
                
            })
            task.resume()

        }
        else{
                print("Network not reachable")
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0
                {                //Do some Action.
                
                }
                
            }
            
        }
    }
 
    func XMLParsingForTop()
    {
        
        let reachability = Reachability()!

        if reachability.isReachable
        {
            self.showHud()
            
            let is_SoapMessage = """
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> <SOAP-ENV:Body> <m:TopSearch xmlns:m="urn:glwsdl"> <limit xsi:type="xsd:string">5</limit> <dictionary xsi:type="xsd:string">Opposites</dictionary> </m:TopSearch> </SOAP-ENV:Body> </soap:Envelope>
"""
            let lobj_Request = NSMutableURLRequest(url: NSURL(string: is_URL)! as URL)
            let session = URLSession.shared
            
            lobj_Request.httpMethod = "POST"
            let soapBody = is_SoapMessage.data(using: String.Encoding.utf8)
            lobj_Request.httpBody = soapBody
            lobj_Request.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            lobj_Request.addValue(String(describing: soapBody!.count), forHTTPHeaderField: "Content-Length")
            
            let task = session.dataTask(with: lobj_Request as URLRequest, completionHandler: {data, response, error -> Void in
                print("Response: \(String(describing: response))")
                do
                {
                    if(response != nil){

                    let xml = try! XML.parse(data!) as? XML.Accessor
                    print(xml!)
                    
                    let strWord = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.childElements.first?.text
                    
                    if let elementsArray = xml?.first.element?.childElements.first?.childElements.first?.childElements.first?.childElements
                    {
                        self.arrForTopSearch.removeAll()
                        for ele in elementsArray
                        {
                            var newElement = [String : String]()
                            let strWord = ele.childElements[0].text!
                            self.arrForTopSearch.insert(strWord, at: self.arrForTopSearch.count)
                        }
                        DispatchQueue.main.async {
                            self.removeHud()
                            self.tbl.reloadData()
                            
                        }
                        
                    }
                    }
                    else{
                        self.removeHud()
                        self.tbl.reloadData()

                    }
                }
                catch
                {
                    print(error.localizedDescription)
                }
                
            })
            task.resume()

        }
        else
        {
            print("Network not reachable")
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }

        
    }

    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionRecentSearchesWords
        {
            return Opposite.sharedInstance.arrRecentOpposite.count

        }
        else
        {
            return Opposite.sharedInstance.arrTopOpposite.count

        }
//        return 5
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        return CGSize(width: collectionView.frame.width/2.5, height: 200);
        return CGSize(width: collectionView.frame.width/2.5, height: 110/140 * (collectionView.frame.width/1.5));
        
    }
    func numberOfSections(in tableView: UITableView) -> Int // Default is 1 if not implemented
    {
        return 1
    }

//    func tableView(_ tableView: UITableView, numberOfSections section: Int) -> Int {
//    {
//        return 1
//
//        }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(arrForTopSearch.count == 0){
            return 0
        }else{
            return arrForTopSearch.count

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! custCellForTop
        cell.txtText.text = "\(arrForTopSearch[indexPath.row])"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OppositesDetailViewController") as! OppositesDetailViewController
        //self.navigationItem.leftBarButtonItems = nil
        //self.navigationController?.pushViewController(donetViewObj, animated: true)
        txtsearchBox.resignFirstResponder()
        
        if txtsearchBox.text?.characters.count == 0
        {
            txtsearchBox.resignFirstResponder()
        }
        if rechabilityObj.isReachable == true
        {
            txtsearchBox.resignFirstResponder()
            let str = "\(arrForTopSearch[indexPath.row])"
            XMLParsing(strTxtEnter:str)
            txtsearchBox.text = ""
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
        

    }
   /*
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
       
    }
*/
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MyCollectionViewCell

        if collectionView == collectionRecentSearchesWords
        {
            let element = Opposite.sharedInstance.arrRecentOpposite[indexPath.row]
            
            cell.lblTitle.text = element["strWord"]!
            cell.lblType.text = "પ્રકાર : \(String(describing: element["strType"]!))"
            cell.lblOppositeTitle.text = "વિપરીત શબ્દ : \(String(describing: element["strOppword"]!))"
            cell.lblOppsiteType.text = "પ્રકાર : \(String(describing: element["strOppType"]!))"
        }
        else
        {
//            cell.lblTitle.text = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements.first?.text
//            cell.lblType.text = "પ્રકાર : \(String(describing: Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[2].text!))"
//            cell.lblOppositeTitle.text = "વિપરીત શબ્દ : \(String(describing: Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[3].text!))"
//            cell.lblOppsiteType.text = "પ્રકાર : \(String(describing: Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[4].text!))"
        }
       
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let donetViewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OppositesDetailViewController") as! OppositesDetailViewController
        
        if collectionView == collectionRecentSearchesWords {
            
            let object = Opposite.sharedInstance.arrRecentOpposite[indexPath.row]
            donetViewObj.currentObject = object
            
//        Opposite.sharedInstance.Word = (Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements.first?.text)!
//        Opposite.sharedInstance.meaning = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements[1].text!
//        Opposite.sharedInstance.type = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements[2].text!
//        Opposite.sharedInstance.oppword = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements[3].text!
//        Opposite.sharedInstance.opptype = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements[4].text!
//        Opposite.sharedInstance.oppmeaning = Opposite.sharedInstance.arrRecentOpposite[indexPath.row].childElements[5].text!
        }
        else
        {
//            Opposite.sharedInstance.Word = (Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements.first?.text)!
//            Opposite.sharedInstance.meaning = Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[1].text!
//            Opposite.sharedInstance.type = Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[2].text!
//            Opposite.sharedInstance.oppword = Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[3].text!
//            Opposite.sharedInstance.opptype = Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[4].text!
//            Opposite.sharedInstance.oppmeaning =  Opposite.sharedInstance.arrTopOpposite[indexPath.row].childElements[5].text!

        }
        self.navigationItem.leftBarButtonItems = nil
        self.navigationController?.pushViewController(donetViewObj, animated: true)
    }
    
    // Mark: - TextField Deleagte

    @IBAction func btnSearchPressed(_ sender: Any) {
        txtsearchBox.resignFirstResponder()
        
            if txtsearchBox.text?.characters.count == 0
            {
                txtsearchBox.resignFirstResponder()
            }
            if rechabilityObj.isReachable == true
            {
                if(txtsearchBox.text == ""){
                    
                }else{
                    
                    txtsearchBox.resignFirstResponder()
                    XMLParsing(strTxtEnter:txtsearchBox.text!)
                    txtsearchBox.text = ""
                }
               
                
                
            }
            else
            {
                UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                    //Do some Action.
                    }
                }
            }
            

    }
    func textFieldShouldReturn(_ txtValue: UITextField) -> Bool {
        
        if txtValue == txtsearchBox {
            if txtValue.text?.characters.count == 0
            {
                txtValue.resignFirstResponder()
                return false
            }
            if rechabilityObj.isReachable == true
            {
                
               XMLParsing(strTxtEnter:txtValue.text!)
                txtValue.text = ""
            }
            else
            {
                UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                    //Do some Action.
                    }
                }
            }
         
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "")
        {
            return false
        }
        return true
    }
    func textFieldDidChange(_ txtValue: UITextField)
    {
        if (txtValue.text == "અૈ") {
            txtValue.text = "ઐ"
            txtsearchBox.text = "ઐ"
        }
        if (txtValue.text == "અૅા") || (txtValue.text == "અાૅ") || (txtValue.text == "આૅ") {
            txtValue.text = "ઑ"
            txtsearchBox.text = "ઑ"
        }
        if (txtValue.text == "આૈ") || (txtValue.text == "અૌ") || (txtValue.text == "ઐા") || (txtValue.text == "ઐા") {
            txtValue.text = "ઔ"
            txtsearchBox.text = "ઔ"
        }
        if (txtValue.text == "અા") {
            txtValue.text = "આ"
            txtsearchBox.text = "આ"
        }
        if (txtValue.text == "અે") {
            txtValue.text = "એ"
            txtsearchBox.text = "એ"
        }
        if (txtValue.text == "આે") || (txtValue.text == "એા") || (txtValue.text == "અો") {
            txtValue.text = "ઓ"
            txtsearchBox.text = "ઓ"
        }
        if (txtValue.text == "અૅ") {
            txtValue.text = "ઍ"
            txtsearchBox.text = "ઍ"
        }
        
        var code: String = ""
        if (txtsearchBox.text?.characters.count)! >= 2 {
            //            code = (txtsearchBox.text as? NSString)?.substring(from: txtsearchBox.text.length() - 2)
            
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            //            let index = txtsearchBox.text!.index(txtsearchBox.text!.endIndex, offsetBy: 2)
            
            code = (txtsearchBox.text?.substring(from: index!))!
        }
        if (code == "ાૅ") || (code == "ૅા") {
            code = "ૉ"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        if (code == "ેા") || (code == "ાે") {
            code = "ો"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        
        
    }
    
     // MARK: - Tap Gesture
    @objc func tap(gesture: UITapGestureRecognizer)
    {
        if keyboardSize != CGSize.zero {
            txtsearchBox.resignFirstResponder()
        }
    }
    // MARK: - Key Board Show & Hidden Method
    @objc func keyboardWillShow(_ notification: Notification) {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(OppositesViewController.tap(gesture:)))
        self.view.addGestureRecognizer(recognizer)
        keyboardSize = ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size)!
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        keyboardSize = CGSize.zero
        self.view.removeGestureRecognizer(recognizer)
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet var lblOppositeTitle : UILabel!
    @IBOutlet var lblOppsiteType : UILabel!

}
