//
//  BaseNavigationViewController.swift
//  GL Plus
//
//  Created by Rajat on 09/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MMDrawerController
import MMMaterialDesignSpinner

class BaseNavigationViewController: UIViewController {
    
    var loaderView = UIView()
    let spinner: MMMaterialDesignSpinner = MMMaterialDesignSpinner.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func addLoaderView() {
        loaderView = UIView.init(frame: self.view.bounds)
        loaderView.backgroundColor = UIColor.clear
        self.view.addSubview(loaderView)
        self.loaderView.isHidden = true
    }
    
    func showHud() {
        loaderView.isHidden = false
        spinner.tintColor = UIColor.red
        spinner.center = loaderView.center
        spinner.lineWidth = 4
        self.loaderView.addSubview(spinner)
        spinner.startAnimating()
        self.view.bringSubview(toFront: loaderView)
    }
    
    func removeHud() {
        spinner.stopAnimating()
        loaderView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.changeSideMenuState(enable: true)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(leftSideButtonTapped(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.leftBarButtonItem = item1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.changeSideMenuState(enable: false)
    }
    
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func leftSideButtonTapped(sender: AnyObject) {

        NotificationCenter.default.post(name: Notification.Name(rawValue:"keyboardWillHide"), object: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    
    }
    
//    func pushProfileViewControrller() {
//        let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//        profile.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(profile, animated: false)
//    }
    
}
