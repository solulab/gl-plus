//
//  HelpViewController.swift
//  GL Plus
//
//  Created by Rajat on 29/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HelpViewController: UIViewController, GADBannerViewDelegate, UIWebViewDelegate {

    @IBOutlet weak var gadBannerObj: GADBannerView!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Help"
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]

        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "share"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnSharePress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItems = [barButton1]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        DispatchQueue.main.async {
            bannerView.load(GADRequest())
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSharePress(sender:UIButton)
    {
        let text = "Hey, I found this interesting! Check this out!\n\n"
        let appLink = "https://itunes.apple.com/in/app/bhagwadgomandal/id1068170549?mt=8"
        print(appLink)
        let textToShare = [ text, appLink ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: nil)
        
        
//        let text = "Thanks for sharing App.\n"
//        if let appLink = NSURL(string: SHARELINK)
//        {
//            let textToShare = [ text, appLink ] as [Any]
//            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
//            self.present(activityViewController, animated: true, completion: nil)
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
