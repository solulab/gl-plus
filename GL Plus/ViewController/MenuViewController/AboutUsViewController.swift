//
//  AboutUsViewController.swift
//  GL Plus
//
//  Created by Hetal Govani on 23/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit


class AboutUsViewController: UIViewController,GADBannerViewDelegate {
    @IBOutlet var gadBannerObj: GADBannerView!
    
    @IBOutlet weak var ViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "About Us"
        
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "share"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnSharePress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItems = [barButton1]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        DispatchQueue.main.async {
        bannerView.load(GADRequest())
        }
      //  textFieldHeight.constant = heightForView(text: lblDesc.text!, font: lblDesc.font, width: lblDesc.frame.width) - 200
        ViewHeight.constant =  heightForView(text: lblDesc.text!, font: lblDesc.font, width: lblDesc.frame.width) - 200

    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }

    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSharePress(sender:UIButton)
    {
        let text = "Hey, I found this interesting! Check this out!\n\n"
        let appLink = "https://itunes.apple.com/in/app/bhagwadgomandal/id1068170549?mt=8"
        print(appLink)
        let textToShare = [ text, appLink ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: nil)
        
//        let text = "Hey, I found this interesting! Check this out!\n\n"
//        let appLink = "\(SHARELINK)\(lblWord.text!)&type=1&page=0"
//        print(appLink)
//        let textToShare = [ text, appLink ] as [Any]
//        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = sender       // so that iPads won't crash
//        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView)
    {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView)
    {
        print("adViewWillLeaveApplication")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

