//
//  ContactUsViewController.swift
//  GL Plus
//
//  Created by Hetal Govani on 23/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import GoogleMobileAds
import CoreLocation
import MapKit
import MessageUI
import ReachabilitySwift

let CALLING_SENTENCE = "Would you like to call 079-48909758"
let PHONE_NUMBER = "079-48909758"
let GUJARATI_LEXICON_EMAIL = "info@gujaratilexicon.com"
let CONTACT_LINK = "http://www.gujaratilexicon.com/mobile/"
let GUJARATI_LEXICON = "GujaratiLexicon"
let ADDRESS_LINK = "GujaratiLexicon, 303-A, Aditya Arcade, Nr. Choice Restaurant, Swastik Cross Road, Navrangpura, Ahmedabad"

class ContactUsViewController: UIViewController,GADBannerViewDelegate,MFMailComposeViewControllerDelegate {
    @IBOutlet var gadBannerObj: GADBannerView!
    @IBOutlet weak var map: MKMapView!
    var rechabilityObj = Reachability()!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contact Us"
        
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        DispatchQueue.main.async {
        bannerView.load(GADRequest())
        }
        if rechabilityObj.isReachable == true
        {
            goToLocation()
        }
        else
        {
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }

    }
    func goToLocation() {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString("303-A, Aditya Arcade,Nr. Choice Restaurant,Swastik Cross Road,Navrangpura,Ahmedabad 380009,Gujarat, India")
        {
            placemarks, error in
            let placemark = placemarks?.first
            let lat = placemark?.location?.coordinate.latitude
            let lon = placemark?.location?.coordinate.longitude
            print("Lat: \(String(describing: lat)), Lon: \(String(describing: lon))")
            
            let newPin = MKPointAnnotation()
            newPin.coordinate = (placemark?.location?.coordinate)!
            self.map.addAnnotation(newPin)
            
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            //set region on the map
            self.map.setRegion(region, animated: true)
        }
    }
    @IBAction func btnMailPress(sender : UIButton)
    {
        if( MFMailComposeViewController.canSendMail() )
        {
            let emailTitle = GUJARATI_LEXICON
            let toRecipents = ["info@gujaratilexicon.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setToRecipients(toRecipents)
            mc.navigationBar.barTintColor = UIColor.white
            self.present(mc, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController.init(title: "GL Plus", message: "Please configure your mail account.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        switch result
        {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btncallPress(sender : UIButton)
    {
        let alert = UIAlertController.init(title: GUJARATI_LEXICON, message: CALLING_SENTENCE, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction) in
            guard let number = URL(string: "tel://" + PHONE_NUMBER) else { return }
            UIApplication.shared.open(number)
        }))
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView)
    {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView)
    {
        print("adViewWillLeaveApplication")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
