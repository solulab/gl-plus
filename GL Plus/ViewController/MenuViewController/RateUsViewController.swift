//
//  RateUsViewController.swift
//  GL Plus
//
//  Created by Hetal Govani on 23/10/17.
//  Copyright © 2017 solulab. All rights reserved.


import UIKit
import GoogleMobileAds

class RateUsViewController: UIViewController,GADBannerViewDelegate
{
    @IBOutlet var gadBannerObj: GADBannerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Rate Us"
        
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [barButton]
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        
        bannerView.load(GADRequest())
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView)
    {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView)
    {
        print("adViewWillLeaveApplication")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

