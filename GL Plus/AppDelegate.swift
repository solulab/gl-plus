//
//  AppDelegate.swift
//  GL Plus
//
//  Created by Hetal Govani on 13/10/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import CoreData
import MMDrawerController
import ReachabilitySwift

var selectedMenuItem = 1
//let greenColor = UIColor.init(red: 153/255, green: 196/255, blue: 86/255, alpha: 1)
let greenColor = UIColor.init(red: 40/255, green: 100/255, blue: 161/255, alpha: 1)
let kNAVIGATIONCOLOR = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
let kBARBUTTONCOLOR = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
let ADS_UNIT_ID = "ca-app-pub-9090402171257698/5978728113"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var drawerContainer: MMDrawerController?
   let reachability = Reachability()!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      //  let navBackgroundImage:UIImage! = UIImage()
        sleep(2)
        var mycontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SpinnerViewController") as! SpinnerViewController
        let navigationBarAppearace = UINavigationBar.appearance()
//        application.statusBarStyle = UIStatusBarStyle.lightContent
//
//        navigationBarAppearace.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navigationBarAppearace.shadowImage = UIImage()
//        navigationBarAppearace.isTranslucent = true
//
//        navigationBarAppearace.tintColor = kBARBUTTONCOLOR
//        navigationBarAppearace.barTintColor = kNAVIGATIONCOLOR
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:kBARBUTTONCOLOR,NSAttributedStringKey.font:UIFont(name: "Lato-Light", size: 23)!]
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
//        self.window = UIWindow(frame: UIScreen.main.bounds)
        buildNavigationDrawer()
//        self.window?.rootViewController = SVMenuOptionManager.sharedInstance.slidingPanel
//        self.window?.makeKeyAndVisible()
        return true
    }

    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable
        {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        else
        {
            print("Network not reachable")
            UIAlertController.showAlert(withTitle: "GL Plus", alertMessage: "Please check your connection and try again", buttonArray: ["OK"]) { buttonIndex in if buttonIndex == 0 {
                //Do some Action.
                }
            }
        }
    }
    
    
    func buildNavigationDrawer()
    {
        let mainStoryBoard:UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let mainPage:CustomTabBarViewController = mainStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
        let leftSideMenu:SVMenuViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SVMenuViewController") as! SVMenuViewController
        let leftSideMenuNav = UINavigationController(rootViewController:leftSideMenu)
        
        drawerContainer = MMDrawerController(center: mainPage, leftDrawerViewController: leftSideMenuNav, rightDrawerViewController: nil)
        drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.bezelPanningCenterView
        drawerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView
        window?.rootViewController = drawerContainer
        
    }
    
    func changeSideMenuState(enable: Bool)
    {
        if enable == true
        {
            drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.bezelPanningCenterView
        }
        else
        {
            drawerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.init(rawValue: 0)
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "GL_Plus")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
extension UIAlertController {
    
    class func showAlert(withTitle alertTitle: String, alertMessage: String, buttonArray: NSArray, completion: ((_ buttonIndex : Int) -> ())? = nil){
        
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        for i in 0..<buttonArray.count {
            let alertButton = UIAlertAction(title: (buttonArray[i] as! String), style: .default, handler: { UIAlertAction in
                
                completion!(i)
                
                alertController.dismiss(animated: true, completion: {
                    
                })
            })
            
            alertController.addAction(alertButton)
        }
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // present the view controller
            topController.present(alertController, animated: true, completion: nil)
            
            
        }
    }
    
}

