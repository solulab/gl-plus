//
//  SpinnerViewController.swift
//  GL Plus
//
//  Created by Rajat on 24/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import MMMaterialDesignSpinner

class SpinnerViewController: UIViewController {
    
    @IBOutlet var spinner: MMMaterialDesignSpinner!
    
    override func viewDidLoad() {
    super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    func showIndicator()
    {
        spinner.lineWidth = 1.5
        spinner.tintColor = UIColor.red
        self.view.addSubview(spinner)
        spinner.startAnimating()
    }
    
    func HideIndicator()
    {
        self.view.removeFromSuperview()
        spinner.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
