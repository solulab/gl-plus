//
//  CustomLabel.h
//
//  Created by Ankit on 3/29/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CustomLabel : UILabel

@property (nonatomic) IBInspectable NSInteger border;
@property (nonatomic) IBInspectable UIColor *color;

- (void)alignTop;
- (void)alignBottom;

@end
