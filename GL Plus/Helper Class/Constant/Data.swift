//
//  File.swift
//  GL Plus
//
//  Created by Rajat on 16/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import Foundation
import SwiftyXMLParser

class Opposite {
    
    static let sharedInstance = Opposite()
    var arrRecentOpposite = [[String: String]]()
    var arrTopOpposite = [XML.Element]()
    var Word: String = String()
    var meaning: String = String()
    var type: String = String()
    var oppword: String = String()
    var opptype: String = String()
    var oppmeaning: String = String()
}
class Idioms {
    static let sharedInstance = Idioms()
    var arrRecentIdioms = [[String: String]]()
//    var arrRecentIdioms = [XML.Element]()
    var arrTopIdioms = [XML.Element]()
    var strEnteredWord :String = String()
    var Word: String = String()
    var meaning: String = String()
    var arrWordIdioms = [XML.Element]()
    
}
class phrase {
    static let sharedInstance = phrase()
    var arrRecentphrase = [[String: String]]()
    var arrTopphrase = [XML.Element]()
    var Word: String = String()
    var meaning: String = String()
}
class Thesaurus {
    static let sharedInstance = Thesaurus()
    var arrRecentThesaurus = [[String: String]]()
    var arrTopThesaurus = [XML.Element]()
    var Word: String = String()
    var SuggestedWord1: String = String()
    var SuggestedWord2: String = String()
    var SuggestedWord3: String = String()
}
class Proverbs {
   
    static let sharedInstance = Proverbs()
    var arrRecentProverbs = [[String: String]]()
    var arrTopProverbs = [XML.Element]()
    var Word: String = String()
    var Word2: String = String()
    var meaning: String = String()
    
}

